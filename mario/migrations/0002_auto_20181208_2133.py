# Generated by Django 2.1 on 2018-12-09 03:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mario', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='characters',
            name='gameSeries',
            field=models.TextField(),
        ),
    ]
