from django.db import models

from django.urls import reverse
# Create your models here.


class Characters(models.Model):

    name = models.CharField(max_length=40)
    description = models.TextField()

    gameSeries = models.TextField()

    def __str__(self):
        return self.name


class Subscribers(models.Model):

    firstName = models.CharField(max_length=40)
    lastName = models.CharField(max_length=40)
    email = models.EmailField(max_length=70, blank=True)

    userName = models.ForeignKey(
        'auth.User',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.email

    def get_absolute_url(self):
        return reverse('user_detail', args=[str(self.pk)])
