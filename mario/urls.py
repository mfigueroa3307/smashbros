#  from django.urls import pathlib


from django.urls import path

from .views import (
    CharactersListView,
    CharacterDetailView,
    UserCreateView,
    UserDetailView,
    News,
    NewHome,
)


urlpatterns = [
    path('', NewHome.as_view(), name='newhome'),
    path('news/', News.as_view(), name='news'),
    path('subscribe/', UserCreateView.as_view(), name='subscribe'),
    path('subscribe/<int:pk>/', UserDetailView.as_view(), name='user_detail'),
    path('character/<int:pk>/', CharacterDetailView.as_view(), name='character_detail'),
    #  path('', CharactersListView.as_view(), name='home'),
    path('characterList', CharactersListView.as_view(), name='home'),

]

