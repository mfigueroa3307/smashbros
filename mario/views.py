# from django.shortcuts import render

# using class based views --Gabby
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView
from .models import Characters, Subscribers
# from django.urls import reverse_lazy

# Create your views here.


class CharactersListView(ListView):
    model = Characters
    template_name = 'home.html'


class CharacterDetailView(DetailView):
    model = Characters
    template_name = 'character_detail.html'


class UserCreateView(CreateView):
    model = Subscribers
    template_name = 'subscribe.html'
    fields = ['firstName', 'lastName', 'email', 'userName']


class UserDetailView(DetailView):
    model = Subscribers
    template_name = 'user_detail.html'


class News(ListView):
    model = Characters
    template_name = 'news.html'


class NewHome(ListView):
    model = Characters
    template_name = 'newhome.html'






